import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RymService {

  //Escribir el URL a la cual se hará la petición.
  public URL = 'https://rickandmortyapi.com/api';

  // Inyectar el método HTTPCLIENT en el constructor.
  constructor(private _http : HttpClient) { }

  // Petición (método)
  getPersonajes(){

// La comilla invertida es para concatenar las variables.
    const url = `${this.URL}/character`; //cuando tienes diferentes metodos.
    return this._http.get(url);//Se va a instanciar con el metodo get a la uri.
  }
}
