import { Component, OnInit } from '@angular/core';
import { RymService } from 'src/app/services/RickandMorty/rym.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  //Arreglo se llama Personajes (html home)
  public personajes : any [] = []; //Setar: asignar un valor a algo

  constructor(private _Rym : RymService) { } //se inyecta el servicio en el constructor

  ngOnInit(): void { //Para ciclo de vida del componente
     // Se inicia todo
    this.getPersonajes();
  }

  getPersonajes() {
    this.personajes = [];
    this._Rym.getPersonajes() //instancia del metodo, para optener los elementos.
    //metodo .subscribe este es para lanzar la peticion y devolver una respuesta (promesas).
    .subscribe((data:any) => { //array de objetos de la API.
      console.log(data);
      this.personajes = data.results; //concatenar
      console.log('Personajes SETEADOS',this.personajes);
    });
  }

}
